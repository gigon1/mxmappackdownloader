Downloads Maps and Replays from TM2 ManiaExchange Mappacks and saves them into a local folder
Configured by a config.json File that can look like this:

```
{
  "MappackName": "SmurfsCup Canyon maps current month",
  "MappackId": 105,
  "IncludeWRReplays": true,
  "ParentFolder": "%userprofile%\\documents\\ManiaPlanet\\Maps\\Downloaded",
  "SubfolderFormat": "Test_SSC_%yyyy%_%MM%"
}
```
Parameters in Config.json

- MappackName: Optional. Only used if no MappackId is provided. Searches for mappack name and selects first entry. If wrong entry is selected you may specify the name to get the correct result.
- MappackId: Optional. The Mappack to download the maps from<br>
Although MappackName and MappackId parameters are optional you must provide one of them.
- IncludeWRReplays: If true the current MX WR Replay file for each map will be downloaded and put beside the map file.
- ParentFolder: the parent folder for downloading the files. May contain Environment variables like %userprofile% so you cann set it to your Maniaplanet "Downloads" folder without changing the path for every pc/user
- SubfolderFormat: the subfolder concatenated to the parent Folder.<br>
May contain any wildcard that can be parsed by [DateTime.ToString()](https://docs.microsoft.com/en-us/dotnet/api/system.datetime.tostring?view=net-5.0). <br>
Wildcards must be enclosed by % so %yyyy% gives 2021.
