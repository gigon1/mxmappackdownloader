﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MxMappackDownloader
{
    public class Config
    {
        public string MappackName { get; set; }
        public int? MappackId { get; set; }
        public string ParentFolder { get; set; }
        public string SubfolderFormat { get; set; }
        public bool IncludeWRReplays { get; set; }
        public string TimeSpanOffset { get; set; }
        public bool OverwriteExisting { get; set; }
        public Config()
        {
            MappackName = "SmurfsCup Canyon maps current month";
            MappackId = 105;
            ParentFolder = @"%userprofile%\documents\ManiaPlanet\Maps\Downloaded";
            SubfolderFormat = "SSC_%yyyy%_%MM%";
            IncludeWRReplays = false;
            TimeSpanOffset = "7";
            OverwriteExisting = false;
        }
    }

}
