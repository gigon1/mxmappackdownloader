﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;


namespace MxMappackDownloader
{
    class Program
    {
        const string ConfigFile = "Config.json";
        static Config Currentconfig;
        static void Main(string[] args)
        {
            //read Config if it exists
            if (File.Exists(ConfigFile))
            {
                Currentconfig = ReadConfigFromFile(ConfigFile);
                if (Currentconfig == null)
                {
                    Console.WriteLine("Error Reading Config File");
                    Console.ReadKey();
                    return;
                }
            }
            //create config if none exists
            else
            {
                Console.WriteLine("No Config file found, creating one");
                File.WriteAllText(ConfigFile, JsonConvert.SerializeObject(new Config(), Formatting.Indented));
                Currentconfig = ReadConfigFromFile(ConfigFile);
                if (Currentconfig == null)
                {
                    Console.WriteLine("Error creating Config file exiting");
                    Console.ReadKey();
                    return;
                }
            }
            //very small check if config might be not OK
            if ((Currentconfig.MappackId != null || Currentconfig.MappackName != null) && Currentconfig.ParentFolder != null)
            {
                Console.WriteLine("Config OK");
                var Downloadfolder = Environment.ExpandEnvironmentVariables(Currentconfig.ParentFolder);
                if (Currentconfig.SubfolderFormat != null)
                {
                    //build Downloadfolder from Parentfolder + DateTime parsed Subfolder
                    var Subfolder = ParsePath(Currentconfig.SubfolderFormat, '%', Currentconfig.TimeSpanOffset);
                    Downloadfolder += $"\\{Subfolder}\\".Replace("\\\\", "\\").Trim();
                }
                if (Currentconfig.MappackId == null)
                {
                    Console.WriteLine("No Mappack ID found, searching for mappack by Name");
                    var mappacksFound = MXApiConnector.SearchMappacksByName(Currentconfig.MappackName);
                    if (mappacksFound?.totalItemCount > 0)
                    {
                        Console.WriteLine($"Found {mappacksFound.totalItemCount} Mappacks, selecting: {mappacksFound.results.First().Name}, ID: {mappacksFound.results.First().ID}");
                        Currentconfig.MappackId = mappacksFound.results.First().ID;
                    }
                    else
                    {
                        Console.WriteLine($"no mappack found named \"{Currentconfig.MappackName}\"");
                        Console.ReadKey();
                    }
                }
                //Get Mappack Infos and display them
                var Mappackinfos = MXApiConnector.GetMappackInfo((int)Currentconfig.MappackId);
                var MappackTracks = MXApiConnector.GetMappackMaps((int)Currentconfig.MappackId);
                if (Mappackinfos != null && MappackTracks != null)
                {
                    Console.WriteLine($"\r\n" +
                        $"########### Mappack Information ###########\r\n" +
                        $"#Name: {Mappackinfos.Name}\r\n" +
                        $"#ID: {Mappackinfos.ID}\r\n" +
                        $"#Author: {Mappackinfos.Username}\r\n" +
                        $"#Last Edited: {Mappackinfos.Edited}\r\n" +
                        $"#Mapcount: {Mappackinfos.TrackCount}\r\n" +
                        $"#Maps:\r\n" +
                        $"#################################################################################################\r\n" +
                        String.Format("# {0,-30} {1,-30} {2,16} {3,12} #\r\n", "Mapname", "Author", "Date Added", "   Author Time") +
                        $"#################################################################################################\r\n" +
                        $"# {String.Join("#\r\n# ", MappackTracks.OrderBy(map => map.Added).Select(map => String.Format("{0,-30} {1,-30} {2,-16} {3,14} ", map.Name.Substring(0,Math.Min(30,map.Name.Length)), map.AuthorLogin.Substring(0,Math.Min(30,map.AuthorLogin.Length)), map.Added.ToString(@"g"), TimeSpan.FromMilliseconds(map.AuthorTime).ToString(@"mm\:ss\.fff"))))}#\r\n" +
                        $"#################################################################################################" );
                    if (MappackTracks.Count > 0)
                    {
                        Console.WriteLine($"Press <Enter> to Download these Maps to {Downloadfolder}");
                        Console.ReadLine();
                        //Download Maps to folder
                        MXApiConnector.DownloadMappackMaps((int)Currentconfig.MappackId, Downloadfolder, Currentconfig.IncludeWRReplays, Currentconfig.OverwriteExisting);

                        Console.WriteLine("\r\nDone");
                        Console.ReadKey();
                        return;
                    }
                    else
                    {
                        Console.WriteLine("This Mappack has no public maps to downlaod. Please add published maps to download them");
                        Console.ReadKey();
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Mappack not found. Please enter correct Mappack ID or name to search in config.json");
                    Console.ReadKey();
                    return;
                }
            }
            else
            {
                Console.WriteLine("No MappackId or ParentFolder in Config found");
                Console.ReadKey();
                return;
            }
        }
        static Config ReadConfigFromFile(string ConfigFilePath)
        {
            try
            {
                var Loadedconfig = JsonConvert.DeserializeObject<Config>(File.ReadAllText("Config.json"));
                Console.WriteLine("Loaded Config");
                return Loadedconfig;
            }
            catch (Exception)
            {
                Console.WriteLine("Config file corrupt\r\nPress Any Key to exit");
                Console.ReadKey();
                return null;
            }
        }
        static List<int> GetIndicies(string StringToSearch, char CharToFind)
        {
            var foundIndexes = new List<int>();
            for (int i = StringToSearch.IndexOf(CharToFind); i > -1; i = StringToSearch.IndexOf(CharToFind, i + 1))
            {
                foundIndexes.Add(i);
            }
            return foundIndexes;
        }
        //Replace wildcards with actual DateTime Infos
        static string ParsePath(string PathToParse, char Separator, string offset)
        {
            string ParsedPath = "";
            var indicies = GetIndicies(PathToParse, Separator);
            if (indicies.Count > 0 && indicies.Count % 2 == 0)
            {
                List<string> FolderFragments = new List<string>();
                FolderFragments.Add(PathToParse.Substring(0, indicies[0]));
                for (int i = 0; i < indicies.Count - 1; i = i + 2)
                {
                    FolderFragments.Add((DateTime.Now + TimeSpan.Parse(offset)).ToString(Currentconfig.SubfolderFormat.Substring(indicies[i] + 1, indicies[i + 1] - indicies[i] - 1)));
                    if (i + 2 < indicies.Count)
                    {
                        FolderFragments.Add(PathToParse.Substring(indicies[i + 1] + 1, indicies[i + 2] - indicies[i + 1] - 1));
                    }
                }
                FolderFragments.Add(PathToParse.Substring(indicies.Last() + 1, PathToParse.Length - indicies.Last() - 1));
                ParsedPath = string.Join("", FolderFragments);
            }
            return ParsedPath;
        }
    }
}
