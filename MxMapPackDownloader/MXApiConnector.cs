﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MxMappackDownloader.MxAPIObjects;
using System.IO;
using System.Text.RegularExpressions;

namespace MxMappackDownloader
{
    public static class MXApiConnector
    {
        const string site = "tm.mania.exchange";
        const string userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
        static Regex r = new Regex(string.Format("[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars()))));
        public static MappackInfo GetMappackInfo(int MappackId)
        {
            var url = $"https://{site}/api/mappack/get_info/{MappackId}";
            var result = GetStringResponse(url);
            if (result != null)
            {
                return JsonConvert.DeserializeObject<MappackInfo>(result);
            }
            Console.WriteLine($"Error Getting Mappack Infos");
            return null;
        }
        public static MXSearchResults<MappackInfo> SearchMappacksByName(string MappackName)
        {
            var url = $"https://{site}/mappacksearch/search?api=on&name={MappackName}";
            var result = GetStringResponse(url);
            if (result != null)
            {
                return JsonConvert.DeserializeObject<MXSearchResults<MappackInfo>>(result);
            }
            Console.WriteLine($"Error searching for Mappack Name");
            return null;
        }
        public static List<MapInfo> GetMappackMaps(int MappackId)
        {
            var url = $"https://{site}/api/mappack/get_mappack_tracks/{MappackId}";
            var result = GetStringResponse(url);
            if (result != null)
            {
                return JsonConvert.DeserializeObject<List<MapInfo>>(result);
            }
            Console.WriteLine($"Error Downloading MapsInfos");
            return null;
        }
        static string GetStringResponse(string url)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", userAgent);
                    using (HttpResponseMessage response = httpClient.GetAsync(url).Result)
                    {
                        var byteArray = response.Content.ReadAsByteArrayAsync().Result;
                        return Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error Getting String from {url}\r\n: {ex.Message}");
                Console.ResetColor();
                return null;
            }
        }
        public static void DownloadMappackMaps(int MappackId, string DownloadPath, bool IncludeWRs, bool OverwriteExistingFiles)
        {
            var MapsInfos = GetMappackMaps(MappackId);
            if (MapsInfos.Count > 0)
            {
                foreach (var map in MapsInfos)
                {
                    var mapname = map.Name;
                    var username = map.ReplayWRUsername;
                    try
                    {

                        if (!Directory.Exists(DownloadPath))
                        {
                            Directory.CreateDirectory(DownloadPath);
                        }                        
                        mapname = r.Replace(map.Name, "_");
                        if (!String.IsNullOrWhiteSpace(map.ReplayWRUsername))
                        {
                            username = r.Replace(map.ReplayWRUsername, "_");
                        }
                        Console.WriteLine($"Downloading Map {mapname}");
                        DownloadMap(map.TrackID, DownloadPath + mapname + ".Gbx", OverwriteExistingFiles);
                        if (IncludeWRs && map.ReplayWRID != null)
                        {
                            DownloadReplay((int)map.ReplayWRID, $"{DownloadPath}{mapname}_{username}({TimeSpan.FromMilliseconds((int)map.ReplayWRTime).ToString(@"\'mm\'ss\'fff")}).Replay.Gbx", OverwriteExistingFiles);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Error Downloading Map: {ex.Message}");
                        Console.ResetColor();
                    }
                }
            }
        }
        public static void DownloadMap(int MapId, string Outputfile, bool OverwriteExistingFile)
        {
            var url = $"https://{site}/maps/download/{MapId}";
            if (File.Exists(Outputfile) && !OverwriteExistingFile)
            {                
                return;
            }
            DownloadBinary(url, Outputfile);
        }
        public static void DownloadReplay(int ReplayId, string Outputfile, bool OverwriteExistingFile)
        {
            var url = $"https://{site}/replays/download/{ReplayId}";
            if (File.Exists(Outputfile) && !OverwriteExistingFile)
            {                
                return;
            }
            DownloadBinary(url, Outputfile);
        }
        static bool DownloadBinary(string url, string Outputfile)
        {
            try
            {                
                using (var httpClient = new HttpClient())
                using (var file = File.OpenWrite(Outputfile))
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", userAgent);
                    httpClient.GetStreamAsync(new Uri(url)).Result.CopyTo(file);
                    Console.WriteLine($"Saved {Outputfile}");
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error Downloading {Outputfile}: {ex.Message}");
                Console.ResetColor();
                return true;
            }
            return false;
        }
    }
}
