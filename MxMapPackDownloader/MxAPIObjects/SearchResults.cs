﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MxMappackDownloader.MxAPIObjects
{
    public class MXSearchResults<T>
    {
            public List<T> results { get; set; }
            public int totalItemCount { get; set; }        
    }
}
