﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MxMappackDownloader.MxAPIObjects
{
    public class MappackInfo
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string StyleName { get; set; }
        public string Titlepack { get; set; }
        public bool Unreleased { get; set; }
        public bool TrackUnreleased { get; set; }
        public bool Downloadable { get; set; }
        public bool TrackHidden { get; set; }
        public bool TrackDownloadable { get; set; }
        public int Environment { get; set; }
        public string EnvironmentName { get; set; }
        public int Downloads { get; set; }
        public bool Request { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public bool Active { get; set; }
        public string ShortName { get; set; }
        public string VideoURL { get; set; }
        public int TrackCount { get; set; }
        public double MappackValue { get; set; }
        public bool ShowLB { get; set; }
        public object EndDateLB { get; set; }
        public object EndDateLBString { get; set; }
    }
}
